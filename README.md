# Offline Contacts Sync App

##### An Ionic + AngularJS + PouchDB + CouchDB application for syncing offline data with a remote server.
 
### Requirements
 
* Node JS
* Ionic
* Cordova
* iOS Sim
* iOS Deploy
* Gulp
* Bower
* Sass
* Android SDK and Build Tools
* XCode

### Technologies
 
* Ionic 1.3.2
* Angular JS 1.5.3
* PouchDB 6.0.5
* PouchDB Authentication 0.5.5
* CouchDB 1.6.1 (local and remote server)

### Installation

##### Node Modules
```bash
npm install
```
##### Bower Dependencies
```bash
bower install
```
##### Restore Platforms and Plugins
```bash
ionic state restore
```
##### Generate Splash Screen and Icon Resources
```bash
ionic resources
```
##### Setup SASS
```bash
ionic setup sass
```
##### Compile CSS styles from SASS files
```bash
gulp sass
```

### Development

##### Run web server on browser
```bash
ionic serve
```

##### Run web server on browser in Lab Mode (side by side platform differences) 
```bash
ionic lab
```

### Build
##### Android
(Must have installed and configured the Android SDK and Android Build Tools)
```bash
ionic build android
```
##### iOS
(Must have developer provision profile activated through XCode)
```bash
ionic build ios
```
### Emulation
##### Android
(Must have installed and configured the Android SDK and an Android Virtual Device)
```bash
ionic emulate android
```
##### iOS
In this example we will run in live-reload mode and with target iPhone 5s with iOS 10.2
```bash
ionic emulate ios --target="iPhone-5s, 10.2" --livereload
```

### Run on Devices
##### Android
(Must have connected a device running Android and activated the **Debug Mode** in the **Developer Settings** of that device)
```bash
ionic run android
```
##### iOS
(Must have developer provision profile activated through XCode)
```bash
ionic run ios
```