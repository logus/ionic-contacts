(function () {
  'use strict';

  angular
    .module('app.state')
    .config(config);

  config.$inject = ['$urlRouterProvider'];

  function config($urlRouterProvider) {
    $urlRouterProvider.otherwise('/login');
  }

})();
