(function () {
  'use strict';
  angular
    .module('app.core')
    .run(block);

  block.$inject = ['$rootScope', '$ionicPlatform', '$cordovaKeyboard'];

  function block($rootScope, $ionicPlatform, $cordovaKeyboard) {
    $ionicPlatform.ready(function () {
      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        $cordovaKeyboard.hideAccessoryBar(true);
        $cordovaKeyboard.disableScroll(true);
      }
      if (window.StatusBar) {
        StatusBar.styleLightContent();
      }
    });

    $ionicPlatform.registerBackButtonAction(function (e) {
      e.preventDefault();
      return false;
    }, 101);

    $rootScope.$on('$stateChangeStart', function (event, toState) {
        if (toState.resolve) {
          $rootScope.$broadcast('loading:show');
        }
      }
    );

    $rootScope.$on('$stateChangeSuccess', function () {
        $rootScope.$broadcast('loading:hide');
      }
    );

  }

})();
