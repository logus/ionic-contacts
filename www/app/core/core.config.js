(function () {
  'use strict';

  angular
    .module('app.core')
    .config(config);

  config.$inject = ['$ionicConfigProvider'];

  function config($ionicConfigProvider) {

    $ionicConfigProvider.backButton.text('').icon('ion-ios-arrow-back');
    $ionicConfigProvider.backButton.previousTitleText(false);
    $ionicConfigProvider.views.swipeBackEnabled(false);
    $ionicConfigProvider.form.checkbox('ios').toggle('ios');
    $ionicConfigProvider.spinner.icon('ios');
  }

})();
