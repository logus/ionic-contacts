(function () {
  'use strict';

  angular
    .module('app')
    .constant('COUCH', 'http://52.204.155.245:5984/')
    .constant('DB_NAME', 'contacts')
    .constant('SYNC_OPTIONS', {live: true, retry: true})
    .constant('COUCH_OPTIONS', {auto_compaction: true})
    .constant('DOC_OPTIONS', {
      include_docs: true,
      attachments: true,
      binary: true,
      live: true
    })
  ;

})();
