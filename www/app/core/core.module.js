(function () {
  'use strict';
  angular.module('app.core', [
    'ionic',
    'ngCordova',
    'pouchdb',
    'ngFileUpload',
    'angular-storage'
  ]);
})();
