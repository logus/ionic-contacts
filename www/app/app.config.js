(function () {
  'use strict';

  angular
    .module('app')
    .config(config);

  config.$inject = ['$compileProvider', 'pouchDBProvider', 'POUCHDB_METHODS'];

  function config($compileProvider, pouchDBProvider, POUCHDB_METHODS) {

    $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|blob|content):/);

    var authMethods = {
      login: 'qify',
      logout: 'qify',
      getSession: 'qify',
      getUser: 'qify',
      putUser: 'qify',
      changePassword: 'qify'
    };

    pouchDBProvider.methods = angular.extend({}, POUCHDB_METHODS, authMethods);

  }

})();
