(function () {
  'use strict';
  angular
    .module('app.account')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {

    var states = [
      {
        name: 'menu.account',
        url: '/account',
        data: {auth: true},
        views: {
          'menu': {
            templateUrl: 'app/account/account.html',
            controller: 'AccountController'
          }
        },
        resolve: {
          currentUser: ['store', function (store) {
            return store.get('currentUser');
          }]
        }
      },
      {
        name: 'menu.changePassword',
        url: '/change-password',
        data: {auth: true},
        views: {
          'menu': {
            templateUrl: 'app/account/account.change-password.html',
            controller: 'AccountChangePasswordController'
          }
        },
        resolve: {
          currentUser: ['store', function (store) {
            return store.get('currentUser');
          }]
        }
      },
      {
        name: 'menu.profile',
        url: '/profile',
        data: {auth: true},
        views: {
          'menu': {
            templateUrl: 'app/account/account.profile.html',
            controller: 'AccountProfileController'
          }
        },
        resolve: {
          currentUser: ['store', 'authService', function (store, authService) {
            return authService.getUser(store.get('currentUser').name);
          }]
        }
      }
    ];

    states.forEach($stateProvider.state);

  }

})();
