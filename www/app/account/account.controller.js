(function () {
  'use strict';
  angular
    .module('app.account')
    .controller('AccountController', AccountController);

  AccountController.$inject = ['$scope', 'currentUser', 'store'];

  function AccountController($scope, currentUser, store) {

    $scope.remember = store.get('remember');
    $scope.setRemember = setRemember;
    $scope.init = init;
    $scope.init();

    function init() {
      setUser();
    }

    function setUser() {
      $scope.currentUser = currentUser;
    }

    function setRemember(remember) {
      store.set('remember', remember);
    }

  }

})();
