(function () {
  'use strict';
  angular
    .module('app.account')
    .controller('AccountProfileController', AccountProfileController);

  AccountProfileController.$inject = ['$scope', '$ionicLoading', '$ionicActionSheet', '$ionicPopup', 'authService', 'currentUser', 'store'];

  function AccountProfileController($scope, $ionicLoading, $ionicActionSheet, $ionicPopup, authService, currentUser, store) {

    $scope.chooseGender = chooseGender;
    $scope.updateProfile = updateProfile;
    $scope.upload = upload;
    $scope.init = init;
    $scope.init();

    function init() {
      setUser();
    }

    function setUser() {
      $scope.currentUser = currentUser;
      if ($scope.currentUser.hasOwnProperty('birthday')) {
        $scope.currentUser.birthday = new Date($scope.currentUser.birthday);
      }
    }

    function chooseGender() {
      var buttons = [
        {text: 'Man'},
        {text: 'Woman'}
      ];
      var hideSheet = $ionicActionSheet.show({
        buttons: buttons,
        titleText: 'Choose Gender',
        cancelText: 'Cancel',
        buttonClicked: function (index) {
          $scope.currentUser.gender = buttons[index].text;
          hideSheet();
        }
      });

    }

    function upload(file) {
      blobToDataURL(file, setUserImage);
      function setUserImage(blob) {
        $scope.currentUser.image = blob;
      }
    }

    function blobToDataURL(blob, callback) {
      var a = new FileReader();
      a.onload = function (e) {
        callback(e.target.result);
      };
      a.readAsDataURL(blob);
    }

    function getUser() {
      authService.getUser($scope.currentUser.name)
        .then(setUser)
        .catch(showError);
    }

    function updateProfile() {

      $ionicLoading.show();

      var metadata = {
        fullName: $scope.currentUser.fullName,
        email: $scope.currentUser.email,
        birthday: $scope.currentUser.birthday,
        gender: $scope.currentUser.gender
      };

      if ($scope.currentUser.hasOwnProperty('image')) {
        metadata.image = $scope.currentUser.image;
      }

      authService.putUser($scope.currentUser.name, metadata)
        .then(successUpdateProfile)
        .catch(showError)
        .finally($ionicLoading.hide);
    }

    function successUpdateProfile() {
      store.set('currentUser', $scope.currentUser);
      $ionicPopup.alert({
        title: 'Profile Updated',
        template: 'Your profile was successfully updated.'
      });
    }

    function showError(error) {
      $ionicPopup.alert({
        title: error.name.charAt(0).toUpperCase() + error.name.slice(1),
        template: error.message
      });
    }


  }

})();
