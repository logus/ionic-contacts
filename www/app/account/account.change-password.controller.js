(function () {
  'use strict';
  angular
    .module('app.account')
    .controller('AccountChangePasswordController', AccountChangePasswordController);

  AccountChangePasswordController.$inject = ['$scope', '$state', '$ionicLoading', '$ionicPopup', 'authService', 'currentUser', 'store'];

  function AccountChangePasswordController($scope, $state, $ionicLoading, $ionicPopup, authService, currentUser, store) {

    $scope.password = {
      newPassword: '',
      confirmNewPassword: ''
    };

    $scope.changePassword = changePassword;
    $scope.init = init;
    $scope.init();

    function init() {
      setUser();
    }

    function setUser() {
      $scope.currentUser = currentUser;
    }

    function changePassword() {
      $ionicLoading.show();

      authService.changePassword($scope.currentUser, $scope.password.newPassword)
        .then(passwordUpdated)
        .catch(showError)
        .finally($ionicLoading.hide);

      function passwordUpdated(user) {
        $scope.currentUser.rev = user.rev;
        store.set('currentUser', $scope.currentUser);
        $ionicPopup.alert({
          title: 'Password Updated',
          template: 'The password was successfully updated.'
        }).then(handleOk);
      }

      function handleOk(res) {
        if (res) {
          $state.go('menu.account');
        }
      }

    }

    function showError(error) {
      $ionicPopup.alert({
        title: error.name.charAt(0).toUpperCase() + error.name.slice(1),
        template: error.message
      });
    }
  }

})();
