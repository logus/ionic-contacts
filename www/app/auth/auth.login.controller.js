(function () {
  'use strict';
  angular
    .module('app.auth')
    .controller('AuthLoginController', AuthLoginController);

  AuthLoginController.$inject = ['$scope', '$timeout', '$state', '$ionicPopup', '$ionicLoading', 'authService', 'store'];

  function AuthLoginController($scope, $timeout, $state, $ionicPopup, $ionicLoading, authService, store) {

    $scope.credentials = {username: '', password: ''};
    $scope.currentUser = store.get('currentUser');
    $scope.remember = store.get('remember');

    $scope.login = login;
    $scope.resume = resume;
    $scope.switchUser = switchUser;
    $scope.setRemember = setRemember;


    function login() {

      $ionicLoading.show();
      authService.authenticate($scope.credentials)
        .then(handleSuccessLogin)
        .catch(showError);

      function handleSuccessLogin() {
        authService.getUser($scope.credentials.username)
          .then(setUser)
          .catch(showError);
      }

      function setUser(user) {
        $scope.currentUser = user;
        if ($scope.remember) {
          store.set('credentials', $scope.credentials);
        }
        store.set('currentUser', user);
        $state.go('menu.contacts')
      }

    }

    function resume() {
      var storedCredentials = store.get('credentials');
      $ionicLoading.show();
      authService.authenticate(storedCredentials)
        .then(handleSuccessLogin)
        .catch(showError);

      function handleSuccessLogin() {
        authService.getUser(storedCredentials.username)
          .then(setUser)
          .catch(showError);
      }

      function setUser(user) {
        $scope.currentUser = user;
        store.set('currentUser', user);
        $state.go('menu.contacts')
      }
    }

    function setRemember(remember) {
      store.set('remember', remember);
    }

    function showError(error) {
      $ionicLoading.hide();
      $ionicPopup.alert({
        title: error.name.charAt(0).toUpperCase() + error.name.slice(1),
        template: error.message
      });
    }

    function switchUser() {
      $ionicLoading.show();
      $timeout(function () {
        store.remove('credentials');
        store.remove('currentUser');
        $scope.credentials = {username: '', password: ''};
        $scope.currentUser = null;
        $ionicLoading.hide();
      }, 1000)
    }

  }

})();
