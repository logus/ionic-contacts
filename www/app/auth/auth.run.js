(function () {
  'use strict';

  angular
    .module('app.auth')
    .run(runBlock);

  runBlock.$inject = ['$rootScope', '$state', 'authService', 'store'];

  function runBlock($rootScope, $state, authService, store) {

    $rootScope.$on('$stateChangeStart', handleStateStart);

    function handleStateStart(event, toState) {

      authService.isAuthenticated()
        .then(authenticated)
        .catch(unauthenticated);

      function authenticated() {
        if (toState.name === 'login') {
          event.preventDefault();
          $state.go('menu.contacts');
        }
      }

      function unauthenticated() {
        if (toState.data) {
          if (toState.data.hasOwnProperty('auth')) {
            if (toState.data.auth) {
              event.preventDefault();
              store.remove('currentUser');
              $state.go('login');
            }
          }
        }
      }

    }

  }

})();
