(function () {
  'use strict';
  angular
    .module('app.auth')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {

    var states = [
      {
        name: 'login',
        url: '/login',
        templateUrl: 'app/auth/auth.login.html',
        controller: 'AuthLoginController'
      }
    ];

    states.forEach($stateProvider.state);

  }

})();
