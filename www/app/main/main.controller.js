(function () {
  'use strict';

  angular
    .module('app.main')
    .controller('MainController', MainController);

  MainController.$inject = ['$rootScope', '$scope', '$ionicLoading'];

  function MainController($rootScope, $scope, $ionicLoading) {

    var main = this;
    main.connection = true;

    main._init = onInit;
    main._init();

    $scope.$on('loading:show', showLoading);
    $scope.$on('loading:hide', hideLoading);

    function onInit() {
      getConnection();
    }

    function getConnection() {

      if (ionic.Platform.isWebView()) {
        $scope.$on('$cordovaNetwork:online', function () {
          main.connection = true;
          $rootScope.$broadcast('serverOnline');
        });
        $scope.$on('$cordovaNetwork:offline', function () {
          main.connection = false;
          $rootScope.$broadcast('serverOffline');
        });
      }
      else {
        window.addEventListener('online', function () {
          main.connection = true;
          $rootScope.$broadcast('serverOnline');
        }, false);

        window.addEventListener('offline', function () {
          main.connection = false;
          $rootScope.$broadcast('serverOffline');
        }, false);
      }
    }

    function showLoading() {
      $ionicLoading.show();
    }

    function hideLoading() {
      $ionicLoading.hide();
    }

  }
})();
