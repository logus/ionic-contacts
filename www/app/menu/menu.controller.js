(function () {
  'use strict';
  angular
    .module('app.menu')
    .controller('MenuController', MenuController);

  MenuController.$inject = ['$scope', '$state', '$ionicLoading', '$ionicPopup', 'authService', 'store'];

  function MenuController($scope, $state, $ionicLoading, $ionicPopup, authService, store) {

    $scope.init = init;
    $scope.logout = logout;
    $scope.onDevice = window.cordova;
    $scope.init();

    function init() {
      setUser()
    }

    function setUser() {
      $scope.currentUser = store.get('currentUser');
    }

    function logout() {
      $ionicLoading.show();
      authService.logout()
        .then(redirectLogin)
        .catch(showError)
        .finally($ionicLoading.hide);

      function redirectLogin() {
        var remember = store.get('remember');
        if (!remember) {
          store.remove('credentials');
          store.remove('currentUser');
        }
        $state.go('login');
      }

      function showError(error) {
        $ionicPopup.alert({
          title: error.name.charAt(0).toUpperCase() + error.name.slice(1),
          template: error.message
        });
      }

    }

  }

})();
