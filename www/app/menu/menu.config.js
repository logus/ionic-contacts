(function () {
  'use strict';
  angular
    .module('app.menu')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {

    var states = [
      {
        name: 'menu',
        url: '/menu',
        data: {auth: true},
        templateUrl: 'app/menu/menu.html',
        controller: 'MenuController'
      }
    ];

    states.forEach($stateProvider.state);

  }

})();
