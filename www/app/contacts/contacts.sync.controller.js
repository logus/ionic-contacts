(function () {
  'use strict';
  angular
    .module('app.contacts')
    .controller('ContactsSyncController', ContactsSyncController);

  ContactsSyncController.$inject = ['$scope', '$filter', '$state', '$ionicLoading', '$ionicPopup', '$cordovaContacts', '$cordovaFile', 'pouchDB', 'DB_NAME', 'COUCH_OPTIONS', 'contacts', 'storedContacts'];

  function ContactsSyncController($scope, $filter, $state, $ionicLoading, $ionicPopup, $cordovaContacts, $cordovaFile, pouchDB, DB_NAME, COUCH_OPTIONS, contacts, storedContacts) {

    $scope.all = false;
    $scope.search = '';

    $scope.init = init;
    $scope.clearSearch = clearSearch;
    $scope.loadContacts = loadContacts;
    $scope.toggleContacts = toggleContacts;
    $scope.syncContacts = syncContacts;
    $scope.init();

    function init() {
      $scope.db = pouchDB(DB_NAME, COUCH_OPTIONS);
      setContacts(contacts);
    }


    function setContacts(contacts) {
      $scope.contacts = [];
      $scope.currentContacts = [];
      angular.forEach(storedContacts.rows, function (contact) {
        $scope.currentContacts.push({name: contact.doc.name, face: contact.doc.face, phone: contact.doc.phone});
      });
      angular.forEach(contacts, function (contact) {
        var tempContact = {name: '', face: '', phone: '', selected: false};
        if (contact.hasOwnProperty('phoneNumbers') && angular.isArray(contact.phoneNumbers) && contact.phoneNumbers.length && !$filter('filter')($scope.currentContacts, {phone: contact.phoneNumbers[0].value}).length) {
          tempContact.name = contact.name.formatted;
          tempContact.phone = contact.phoneNumbers[0].value;
          if (contact.hasOwnProperty('photos') && angular.isArray(contact.photos) && contact.photos.length && ionic.Platform.isIOS()) {
            tempContact.image = contact.photos[0].value;
          }
          $scope.contacts.push(tempContact);
        }
      });
    }

    function clearSearch() {
      $scope.search = '';
    }

    function loadContacts() {
      if (window.cordova) {
        var options = {fields: ['name', 'phoneNumbers', 'photos']};
        $cordovaContacts.find(options)
          .then(setContacts)
          .catch(errorLoadContacts)
          .finally(refreshComplete);
      }
    }

    function refreshComplete() {
      $scope.$broadcast('scroll.refreshComplete');
    }

    function errorLoadContacts() {
      $ionicPopup.alert({
        title: 'Error',
        template: 'The application was unable to load the device\'s contacts.'
      });
    }

    function toggleContacts() {
      angular.forEach($filter('filter')($scope.contacts, $scope.search), function (contact) {
        contact.selected = $scope.all;
      });
    }

    function syncContacts() {

      $ionicLoading.show();
      $scope.counter = 1;
      var contacts = $filter('filter')($scope.contacts, {selected: true});

      angular.forEach(contacts, function (contact) {
        if (contact.hasOwnProperty('image')) {
          var imageArray = contact.image.split('/');
          contact.face = imageArray[imageArray.length - 1];
        }
        $scope.db.post({face: contact.face, name: contact.name, phone: contact.phone})
          .then(function (doc) {
            contact.id = doc.id;
            contact.rev = doc.rev;
            if (contact.face.length) {
              attachPhoto(contact);
            } else {
              updateCounter();
            }
          })
          .catch(showError);
      });

    }

    function updateCounter() {
      if ($scope.counter === $filter('filter')($scope.contacts, {selected: true}).length) {
        completeSync();
      } else {
        $scope.counter++;
      }
    }

    function completeSync() {
      $ionicLoading.hide();
      $ionicPopup.alert({
        title: 'Sync Complete',
        template: $filter('filter')($scope.contacts, {selected: true}).length + ' contacts have been synced'
      }).then(resolveCompleteSync);

      function resolveCompleteSync(res) {
        if (res) {
          $state.go('menu.contacts');
        }
      }

    }

    function attachPhoto(contact) {

      $cordovaFile.readAsDataURL(cordova.file.tempDirectory, contact.face)
        .then(fileRead)
        .catch(showError);

      function fileRead(file) {
        file = dataURLtoBlob(file);
        $scope.db.putAttachment(contact.id, contact.face, contact.rev, file, file.type)
          .finally(function () {
            updateCounter();
          })
          .catch(showError);
      }

      function dataURLtoBlob(dataurl) {
        var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
          bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
        while (n--) {
          u8arr[n] = bstr.charCodeAt(n);
        }
        return new Blob([u8arr], {type: mime});
      }

    }

    function showError(error) {
      $ionicLoading.hide();
      $ionicPopup.alert({
        title: error.name.charAt(0).toUpperCase() + error.name.slice(1),
        template: error.message
      });
    }

  }

})();
