(function () {
  'use strict';

  angular
    .module('app.contacts')
    .controller('ContactController', ContactController);

  ContactController.$inject = ['$scope', '$state', '$stateParams', '$ionicLoading', '$ionicPopup', 'pouchDB', 'COUCH', 'DB_NAME', 'DOC_OPTIONS', 'SYNC_OPTIONS', 'COUCH_OPTIONS', 'contact'];

  function ContactController($scope, $state, $stateParams, $ionicLoading, $ionicPopup, pouchDB, COUCH, DB_NAME, DOC_OPTIONS, SYNC_OPTIONS, COUCH_OPTIONS, contact) {

    $scope.updateContact = updateContact;
    $scope.upload = upload;

    $scope._init = _init;
    $scope._init();

    function _init() {
      $scope.db = pouchDB(DB_NAME, COUCH_OPTIONS);
      setContact(contact);
      sync();
    }

    function getContact() {
      $scope.db.get($stateParams.id, DOC_OPTIONS)
        .then(setContact)
        .catch(showError);
    }

    function setContact(contact) {

      $scope.contact = contact;

      if ($scope.contact.hasOwnProperty('_attachments') && angular.isObject($scope.contact._attachments) && $scope.contact._attachments.hasOwnProperty($scope.contact.face)) {
        $scope.contact.image = (window.URL ? URL : webkitURL).createObjectURL($scope.contact._attachments[$scope.contact.face].data);
      }

    }

    function updateContact() {

      $ionicLoading.show();

      $scope.db.put($scope.contact)
        .then(successUpdateContact)
        .catch(showError)
        .finally($ionicLoading.hide);

      function successUpdateContact() {
        $ionicPopup.alert({
          title: 'Saved',
          content: 'The contact was successfully updated.'
        }).then(resolveUpdate);

        function resolveUpdate(res) {
          if (res) {
            $state.go('menu.contacts');
          }
        }
      }

    }

    function upload(file) {

      $scope.db.putAttachment($scope.contact._id, file.$ngfName, $scope.contact._rev, file, file.type)
        .then(handleAttachmentSuccess)
        .catch(showError);

      function handleAttachmentSuccess() {
        $scope.db.get($scope.contact._id, DOC_OPTIONS)
          .then(updateContact)
          .catch(showError);

        function updateContact(contact) {
          $scope.contact = contact;
          $scope.contact.face = file.$ngfName;
        }
      }

    }

    function showError(error) {
      $ionicPopup.alert({
        title: error.name.charAt(0).toUpperCase() + error.name.slice(1),
        template: error.message
      });
    }

    function sync() {
      $scope.db.sync(COUCH + DB_NAME, SYNC_OPTIONS).$promise
        .then(null, null, getContact);
    }

  }

})();
