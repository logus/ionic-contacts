(function () {
  'use strict';

  angular
    .module('app.contacts')
    .controller('ContactAddController', ContactAddController);

  ContactAddController.$inject = ['$scope', '$state', '$ionicLoading', '$ionicPopup', 'pouchDB', 'DB_NAME', 'COUCH_OPTIONS', 'DOC_OPTIONS', 'contact'];

  function ContactAddController($scope, $state, $ionicLoading, $ionicPopup, pouchDB, DB_NAME, COUCH_OPTIONS, DOC_OPTIONS, contact) {

    $scope.cancel = cancel;
    $scope.saveContact = saveContact;
    $scope.upload = upload;

    $scope._init = _init;
    $scope._init();

    function _init() {
      $scope.db = pouchDB(DB_NAME, COUCH_OPTIONS);
      getContact(contact.id)
    }

    function getContact(id) {
      $scope.db.get(id, DOC_OPTIONS)
        .then(successGetContact)
        .catch(showError);

      function successGetContact(contact) {
        $scope.contact = contact;
        if ($scope.contact.hasOwnProperty('_attachments') && angular.isObject($scope.contact._attachments) && $scope.contact._attachments.hasOwnProperty($scope.contact.face)) {
          $scope.contact.image = (window.URL ? URL : webkitURL).createObjectURL($scope.contact._attachments[$scope.contact.face].data);
        }
      }

    }

    function saveContact() {
      $ionicLoading.show();
      $scope.db.put($scope.contact)
        .then(successSaveContact)
        .catch(showError)
        .finally($ionicLoading.hide);

      function successSaveContact() {
        $ionicPopup.alert({
          title: 'Added',
          content: 'The contact was successfully added.'
        }).then(resolveUpdate);

        function resolveUpdate(res) {
          if (res) {
            $state.go('menu.contacts');
          }
        }
      }

    }

    function upload(file) {

      $scope.db.putAttachment($scope.contact._id, file.$ngfName, $scope.contact._rev, file, file.type)
        .then(handleAttachmentSuccess)
        .catch(showError);

      function handleAttachmentSuccess() {
        $scope.db.get($scope.contact._id, DOC_OPTIONS)
          .then(updateContact)
          .catch(showError);

        function updateContact(contact) {
          $scope.contact = contact;
          $scope.contact.face = file.$ngfName;
        }
      }

    }

    function cancel() {
      $scope.db.remove($scope.contact, DOC_OPTIONS)
        .then(redirectContacts)
        .catch(showError);

      function redirectContacts() {
        $state.go('menu.contacts');
      }

    }

    function showError(error) {
      $ionicPopup.alert({
        title: error.name.charAt(0).toUpperCase() + error.name.slice(1),
        template: error.message
      });
    }

  }

})();
