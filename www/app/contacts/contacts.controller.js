(function () {
  'use strict';

  angular
    .module('app.contacts')
    .controller('ContactsController', ContactsController);

  ContactsController.$inject = ['$scope', '$ionicLoading', '$ionicPopup', 'pouchDB', 'COUCH', 'DB_NAME', 'SYNC_OPTIONS', 'DOC_OPTIONS', 'COUCH_OPTIONS', 'contacts', 'authService'];

  function ContactsController($scope, $ionicLoading, $ionicPopup, pouchDB, COUCH, DB_NAME, SYNC_OPTIONS, DOC_OPTIONS, COUCH_OPTIONS, contacts, authService) {

    $scope.showDelete = false;
    $scope.search = '';
    $scope.toggleDelete = toggleDelete;
    $scope.clearSearch = clearSearch;
    $scope.removeContact = removeContact;
    $scope.bulkDelete = bulkDelete;
    $scope.$on('serverOnline', sync);

    $scope.init = init;
    $scope.sync = sync;
    $scope.reset = reset;
    $scope.init();

    function init() {
      $scope.db = pouchDB(DB_NAME, COUCH_OPTIONS);
      successGetContacts(contacts);
      sync();
    }

    function reset() {
      $scope.db.destroy()
        .then(init)
        .catch(showError);

    }

    function toggleDelete() {
      $scope.showDelete = !$scope.showDelete;
    }

    function getContacts() {
      $scope.db.allDocs(DOC_OPTIONS)
        .then(successGetContacts)
        .catch(showError);
      authService.compact();
      $scope.db.compact();
    }

    function successGetContacts(response) {
      $scope.$broadcast('scroll.refreshComplete');
      $scope.docs = response;
      angular.forEach($scope.docs.rows, function (contact) {
        if (contact.doc.hasOwnProperty('_attachments') && angular.isObject(contact.doc._attachments) && contact.doc._attachments.hasOwnProperty(contact.doc.face)) {
          contact.doc.image = (window.URL ? URL : webkitURL).createObjectURL(contact.doc._attachments[contact.doc.face].data);
        }
      });
    }

    function removeContact(contact) {
      $ionicPopup.confirm({
        title: 'Remove Contact',
        template: 'Are you sure you want to remove this contact?'
      }).then(confirmRemoveContact);

      function confirmRemoveContact(res) {
        if (res) {
          $scope.db.remove(contact.doc, DOC_OPTIONS)
            .then(getContacts)
            .catch(showError);
        }
      }

    }

    function clearSearch() {
      $scope.search = '';
    }

    function sync() {
      $scope.db.sync(COUCH + DB_NAME, SYNC_OPTIONS).$promise
        .then(null, null, getContacts);
    }

    function showError(error) {
      $ionicLoading.hide();
      $ionicPopup.alert({
        title: error.name.charAt(0).toUpperCase() + error.name.slice(1),
        template: error.message
      });
    }

    function bulkDelete() {
      $ionicPopup.confirm({
        title: 'Clear All Contacts',
        template: 'This will remove all your contacts from the application.'
      }).then(resolveDelete);

      function resolveDelete(res) {
        if (res) {
          $scope.counter = 1;
          $ionicLoading.show();
          $scope.docs.rows.forEach(function (contact) {
            $scope.db.remove(contact.doc, DOC_OPTIONS)
              .then(updateCounter)
              .catch(showError);
          });
        }
      }

      function updateCounter() {
        if ($scope.counter === $scope.docs.rows.length) {
          $ionicLoading.hide();
          getContacts();
        } else {
          $scope.counter++;
        }
      }

    }

  }

})();
