(function () {
  'use strict';
  angular
    .module('app.contacts')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {

    var states = [
      {
        name: 'menu.contacts',
        url: '/contacts',
        data: {auth: true},
        views: {
          'menu': {
            templateUrl: 'app/contacts/contacts.html',
            controller: 'ContactsController'
          }
        },
        resolve: {
          contacts: ['pouchDB', '$stateParams', 'DB_NAME', 'COUCH_OPTIONS', 'DOC_OPTIONS', function (pouchDB, $stateParams, DB_NAME, COUCH_OPTIONS, DOC_OPTIONS) {
            var db = pouchDB(DB_NAME, COUCH_OPTIONS);
            return db.allDocs(DOC_OPTIONS);
          }]
        }
      },
      {
        name: 'menu.contact',
        url: '/contacts/:id',
        data: {auth: true},
        views: {
          'menu': {
            templateUrl: 'app/contacts/contact.html',
            controller: 'ContactController'
          }
        },
        resolve: {
          contact: ['pouchDB', '$stateParams', 'DB_NAME', 'COUCH_OPTIONS', 'DOC_OPTIONS', function (pouchDB, $stateParams, DB_NAME, COUCH_OPTIONS, DOC_OPTIONS) {
            var db = pouchDB(DB_NAME, COUCH_OPTIONS);
            return db.get($stateParams.id, DOC_OPTIONS);
          }]
        }
      },
      {
        name: 'menu.addContact',
        url: '/add-contact',
        data: {auth: true},
        views: {
          'menu': {
            templateUrl: 'app/contacts/contact.add.html',
            controller: 'ContactAddController'
          }
        },
        resolve: {
          contact: ['pouchDB', 'DB_NAME', 'COUCH_OPTIONS', function (pouchDB, DB_NAME, COUCH_OPTIONS) {
            var db = pouchDB(DB_NAME, COUCH_OPTIONS);
            return db.post({face: '', name: '', phone: ''});
          }]
        }
      },
      {
        name: 'menu.contactsSync',
        url: '/contacts-sync',
        data: {auth: true},
        views: {
          'menu': {
            templateUrl: 'app/contacts/contacts.sync.html',
            controller: 'ContactsSyncController'
          }
        },
        resolve: {
          contacts: ['$cordovaContacts', function ($cordovaContacts) {
            var options = {fields: ['name', 'phoneNumbers', 'photos']};
            return $cordovaContacts.find(options);
          }],
          storedContacts: ['pouchDB', '$stateParams', 'DB_NAME', 'COUCH_OPTIONS', 'DOC_OPTIONS', function (pouchDB, $stateParams, DB_NAME, COUCH_OPTIONS, DOC_OPTIONS) {
            var db = pouchDB(DB_NAME, COUCH_OPTIONS);
            return db.allDocs(DOC_OPTIONS);
          }]
        }
      }
    ];

    states.forEach($stateProvider.state);

  }

})();
