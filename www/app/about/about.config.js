(function () {
  'use strict';
  angular
    .module('app.about')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {

    var states = [
      {
        name: 'about',
        url: '/about',
        templateUrl: 'app/about/about.html',
        controller: 'AboutController'
      },
      {
        name: 'menu.about',
        url: '/about',
        data: {auth: true},
        views: {
          'menu': {
            templateUrl: 'app/about/about.html',
            controller: 'AboutController'
          }
        }
      }
    ];

    states.forEach($stateProvider.state);

  }

})();
