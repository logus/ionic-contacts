(function () {

  angular
    .module('app.network')
    .factory('networkService', networkService);

  networkService.$inject = ['$cordovaNetwork'];

  function networkService($cordovaNetwork) {

    var
      service = {
        isOnline: isOnline,
        isOffline: isOffline
      };

    return service;

    function isOnline() {
      if (ionic.Platform.isWebView()) {
        return $cordovaNetwork.isOnline();
      } else {
        return navigator.onLine;
      }
    }

    function isOffline() {
      if (ionic.Platform.isWebView()) {
        return !$cordovaNetwork.isOnline();
      } else {
        return !navigator.onLine;
      }
    }

  }

})();
