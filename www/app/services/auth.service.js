(function () {
  'use strict';

  angular
    .module('app.auth')
    .factory('authService', authService);

  authService.$inject = ['$q', '$http', 'pouchDB', 'COUCH', 'DB_NAME', 'COUCH_OPTIONS', 'networkService', 'store'];

  function authService($q, $http, pouchDB, COUCH, DB_NAME, COUCH_OPTIONS, networkService, store) {

    var
      db = pouchDB(COUCH + DB_NAME, COUCH_OPTIONS),
      service = {
        authenticate: authenticate,
        logout: logout,
        getUser: getUser,
        isAuthenticated: isAuthenticated,
        putUser: putUser,
        changePassword: changePassword,
        uuid: uuid,
        compact: compact
      };

    return service;

    function authenticate(credentials) {
      var deferred = $q.defer();
      db.login(credentials.username, credentials.password)
        .then(deferred.resolve)
        .catch(deferred.reject);
      return deferred.promise;
    }

    function logout() {
      var deferred = $q.defer();
      db.logout()
        .then(deferred.resolve)
        .catch(deferred.reject);
      return deferred.promise;
    }

    function isAuthenticated() {
      var deferred = $q.defer();

      if (networkService.isOnline()) {
        db.getSession()
          .then(evalResponse)
          .catch(deferred.reject);
      } else {
        var user = store.get('currentUser');
        if (user) {
          deferred.resolve(user);
        } else {
          deferred.reject();
        }
      }

      function evalResponse(obj) {
        if (obj.hasOwnProperty('userCtx') && obj.userCtx.hasOwnProperty('name') && obj.userCtx.name !== null) {
          deferred.resolve(obj);
        } else {
          deferred.reject(obj);
        }
      }

      return deferred.promise;
    }


    function changePassword(user, newPassword) {
      var deferred = $q.defer();

      db.changePassword(user.name, newPassword)
        .then(deferred.resolve)
        .catch(deferred.reject);

      return deferred.promise;
    }

    function uuid() {
      var deferred = $q.defer();

      $http.get(COUCH + '_uuids')
        .then(successUUID)
        .catch(deferred.reject);

      function successUUID(result) {
        var response = result.data;
        if (angular.isObject(response) && response.hasOwnProperty('uuids') && angular.isArray(response.uuids) && response.uuids.length) {
          deferred.resolve(response.uuids[0]);
        } else {
          deferred.reject();
        }
      }

      return deferred.promise;
    }

    function putUser(username, metadata) {

      var deferred = $q.defer();

      db.putUser(username, {metadata: metadata})
        .then(deferred.resolve)
        .catch(deferred.reject);

      return deferred.promise;
    }

    function getUser(username) {
      var deferred = $q.defer();

      db.getUser(username)
        .then(deferred.resolve)
        .catch(deferred.reject);

      return deferred.promise;
    }

    function compact() {
      var deferred = $q.defer();

      db.compact()
        .then(deferred.resolve)
        .catch(deferred.reject);

      return deferred.promise;
    }

  }

})();
