(function () {
  'use strict';

  angular.module('app', [
    'app.core',
    'app.state',
    'app.main',
    'app.auth',
    'app.menu',
    'app.about',
    'app.account',
    'app.network',
    'app.contacts'
  ]);

})();
